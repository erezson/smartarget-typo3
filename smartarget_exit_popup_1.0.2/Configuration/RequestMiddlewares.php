<?php

return [
    'frontend' => [
        'SmartargetExitPopup-frontend' => [
            'target' => \Smartarget\SmartargetExitPopup\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
