<?php

return [
    'frontend' => [
        'SmartargetFacebookMessenger-frontend' => [
            'target' => \Smartarget\SmartargetFacebookMessenger\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
