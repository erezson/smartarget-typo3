<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetFacebookMessenger', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetFacebookMessenger', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetFacebookMessenger\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_facebook_messenger/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_facebook_messenger/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
