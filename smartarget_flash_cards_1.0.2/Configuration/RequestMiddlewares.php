<?php

return [
    'frontend' => [
        'SmartargetFlashCards-frontend' => [
            'target' => \Smartarget\SmartargetFlashCards\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
