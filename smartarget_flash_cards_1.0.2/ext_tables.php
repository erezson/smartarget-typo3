<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetFlashCards', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetFlashCards', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetFlashCards\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_flash_cards/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_flash_cards/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
