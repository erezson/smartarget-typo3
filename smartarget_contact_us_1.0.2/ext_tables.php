<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetContactUs', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetContactUs', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetContactUs\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_contact_us/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_contact_us/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
