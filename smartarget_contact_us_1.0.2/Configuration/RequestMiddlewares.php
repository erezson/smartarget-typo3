<?php

return [
    'frontend' => [
        'SmartargetContactUs-frontend' => [
            'target' => \Smartarget\SmartargetContactUs\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
