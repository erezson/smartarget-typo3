<?php

return [
    'frontend' => [
        'SmartargetCallButton-frontend' => [
            'target' => \Smartarget\SmartargetCallButton\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
