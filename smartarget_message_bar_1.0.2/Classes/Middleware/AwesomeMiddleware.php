<?php
namespace Smartarget\SmartargetMessageBar\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Core\Database\ConnectionPool;


class AwesomeMiddleware implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $domain = $_SERVER['HTTP_HOST'];
        $hash = sha1(sha1("typo3_message_bar_" . preg_replace("/www\.|https?:\/\/|\/$|\/?\?.+|\/.+|^\./", '', $domain)) . "_script");
        $script = '<script type="text/javascript" src="https://smartarget.online/loader.js?u=' . $hash . '&source=typo3_message_bar"></script>';
        $html = $response->getBody();
        $html = str_replace("</body>","$script</body>",$html);

        $body = new Stream('php://temp', 'wb+');

        $body->write($html);
        $response = $response->withBody($body);

        return $response;
    }
}
