<?php

return [
    'frontend' => [
        'SmartargetMessageBar-frontend' => [
            'target' => \Smartarget\SmartargetMessageBar\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
