<?php

return [
    'frontend' => [
        'SmartargetTiktokFollow-frontend' => [
            'target' => \Smartarget\SmartargetTiktokFollow\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
