<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['smartarget_tiktok_follow'] = 'EXT:smartarget_tiktok_follow/Configuration/RTE/Default.yaml';



$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionUninstall',
        \Smartarget\SmartargetTiktokFollow\Hooks\AppMethods::class,
        'removeApp'
);

$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionInstall',
        \Smartarget\SmartargetTiktokFollow\Hooks\AppMethods::class,
        'addApp'
);

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Smartarget.SmartargetTiktokFollow',
            'Ajaxmap',
            [
                \Smartarget\SmartargetTiktokFollow\Controller\PostController::class => 'main'
            ],
            // non-cacheable actions
            [
                \Smartarget\SmartargetTiktokFollow\Controller\PostController::class => 'main'
            ]
        );

    }
);
