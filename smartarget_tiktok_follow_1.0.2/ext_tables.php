<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetTiktokFollow', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetTiktokFollow', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetTiktokFollow\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_tiktok_follow/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_tiktok_follow/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
