<?php

/**
 * Extension Manager/Repository config file for ext "smartarget_tiktok_follow".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Smartarget Tiktok - Follow Us',
    'description' => 'Get more Tiktok subscribers',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            //'typo3' => '9.3.0-10.4.99'
            'typo3' => '11.0.0-11.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Smartarget\\SmartargetTiktokFollow\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Erez',
    'author_email' => 'support@smartarget.online',
    'author_company' => 'Smartarget',
    'version' => '1.0.2',
];
