<?php

return [
    'frontend' => [
        'SmartargetInstagramFollow-frontend' => [
            'target' => \Smartarget\SmartargetInstagramFollow\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
