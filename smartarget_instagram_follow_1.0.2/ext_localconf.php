<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['smartarget_instagram_follow'] = 'EXT:smartarget_instagram_follow/Configuration/RTE/Default.yaml';



$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionUninstall',
        \Smartarget\SmartargetInstagramFollow\Hooks\AppMethods::class,
        'removeApp'
);

$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionInstall',
        \Smartarget\SmartargetInstagramFollow\Hooks\AppMethods::class,
        'addApp'
);

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Smartarget.SmartargetInstagramFollow',
            'Ajaxmap',
            [
                \Smartarget\SmartargetInstagramFollow\Controller\PostController::class => 'main'
            ],
            // non-cacheable actions
            [
                \Smartarget\SmartargetInstagramFollow\Controller\PostController::class => 'main'
            ]
        );

    }
);
