<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetInstagramFollow', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetInstagramFollow', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetInstagramFollow\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_instagram_follow/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_instagram_follow/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
