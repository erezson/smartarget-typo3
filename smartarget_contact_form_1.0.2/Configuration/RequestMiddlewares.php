<?php

return [
    'frontend' => [
        'SmartargetContactForm-frontend' => [
            'target' => \Smartarget\SmartargetContactForm\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
