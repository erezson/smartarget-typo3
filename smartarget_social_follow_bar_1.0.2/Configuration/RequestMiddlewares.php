<?php

return [
    'frontend' => [
        'SmartargetSocialFollowBar-frontend' => [
            'target' => \Smartarget\SmartargetSocialFollowBar\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
