<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetSocialFollowBar', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetSocialFollowBar', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetSocialFollowBar\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_social_follow_bar/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_social_follow_bar/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
