<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['smartarget_button_builder'] = 'EXT:smartarget_button_builder/Configuration/RTE/Default.yaml';



$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionUninstall',
        \Smartarget\SmartargetButtonBuilder\Hooks\AppMethods::class,
        'removeApp'
);

$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionInstall',
        \Smartarget\SmartargetButtonBuilder\Hooks\AppMethods::class,
        'addApp'
);

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Smartarget.SmartargetButtonBuilder',
            'Ajaxmap',
            [
                \Smartarget\SmartargetButtonBuilder\Controller\PostController::class => 'main'
            ],
            // non-cacheable actions
            [
                \Smartarget\SmartargetButtonBuilder\Controller\PostController::class => 'main'
            ]
        );

    }
);
