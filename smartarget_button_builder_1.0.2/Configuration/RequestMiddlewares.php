<?php

return [
    'frontend' => [
        'SmartargetButtonBuilder-frontend' => [
            'target' => \Smartarget\SmartargetButtonBuilder\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
