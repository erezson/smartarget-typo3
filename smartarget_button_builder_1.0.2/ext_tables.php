<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetButtonBuilder', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetButtonBuilder', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetButtonBuilder\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_button_builder/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_button_builder/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
