<?php

return [
    'frontend' => [
        'SmartargetEmail-frontend' => [
            'target' => \Smartarget\SmartargetEmail\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
