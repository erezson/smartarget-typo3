<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetFacebookFollow', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetFacebookFollow', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetFacebookFollow\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_facebook_follow/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_facebook_follow/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
