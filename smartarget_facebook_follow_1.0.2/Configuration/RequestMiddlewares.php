<?php

return [
    'frontend' => [
        'SmartargetFacebookFollow-frontend' => [
            'target' => \Smartarget\SmartargetFacebookFollow\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
