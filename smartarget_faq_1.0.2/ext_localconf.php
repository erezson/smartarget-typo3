<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['smartarget_faq'] = 'EXT:smartarget_faq/Configuration/RTE/Default.yaml';



$signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionUninstall',
        \Smartarget\SmartargetFaq\Hooks\AppMethods::class,
        'removeApp'
);

$signalSlotDispatcher->connect(
        \TYPO3\CMS\Extensionmanager\Utility\InstallUtility::class,  // Signal class name
        'afterExtensionInstall',
        \Smartarget\SmartargetFaq\Hooks\AppMethods::class,
        'addApp'
);

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Smartarget.SmartargetFaq',
            'Ajaxmap',
            [
                \Smartarget\SmartargetFaq\Controller\PostController::class => 'main'
            ],
            // non-cacheable actions
            [
                \Smartarget\SmartargetFaq\Controller\PostController::class => 'main'
            ]
        );

    }
);
