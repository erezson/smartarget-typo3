<?php

return [
    'frontend' => [
        'SmartargetFaq-frontend' => [
            'target' => \Smartarget\SmartargetFaq\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
