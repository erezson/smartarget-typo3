<?php

return [
    'frontend' => [
        'SmartargetTelegram-frontend' => [
            'target' => \Smartarget\SmartargetTelegram\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
