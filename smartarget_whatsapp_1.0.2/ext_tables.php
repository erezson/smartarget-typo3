<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetWhatsapp', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetWhatsapp', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetWhatsapp\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_whatsapp/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_whatsapp/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
