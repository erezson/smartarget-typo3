<?php

return [
    'frontend' => [
        'SmartargetWhatsapp-frontend' => [
            'target' => \Smartarget\SmartargetWhatsapp\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
