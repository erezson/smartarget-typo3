<?php

return [
    'frontend' => [
        'SmartargetSkype-frontend' => [
            'target' => \Smartarget\SmartargetSkype\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
