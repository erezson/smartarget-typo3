<?php

/**
 * Extension Manager/Repository config file for ext "smartarget_viber".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Smartarget Viber - Contact Us',
    'description' => 'Allow customers to contact you using Viber',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            //'typo3' => '9.3.0-10.4.99'
            'typo3' => '11.0.0-11.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Smartarget\\SmartargetViber\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Erez',
    'author_email' => 'support@smartarget.online',
    'author_company' => 'Smartarget',
    'version' => '1.0.2',
];
