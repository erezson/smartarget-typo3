<?php

return [
    'frontend' => [
        'SmartargetViber-frontend' => [
            'target' => \Smartarget\SmartargetViber\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
