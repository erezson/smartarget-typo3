<?php

return [
    'frontend' => [
        'SmartargetLine-frontend' => [
            'target' => \Smartarget\SmartargetLine\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
