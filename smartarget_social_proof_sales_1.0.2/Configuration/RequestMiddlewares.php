<?php

return [
    'frontend' => [
        'SmartargetSocialProofSales-frontend' => [
            'target' => \Smartarget\SmartargetSocialProofSales\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
