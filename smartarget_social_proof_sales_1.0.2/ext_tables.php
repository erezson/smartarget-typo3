<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetSocialProofSales', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetSocialProofSales', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetSocialProofSales\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_social_proof_sales/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_social_proof_sales/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
