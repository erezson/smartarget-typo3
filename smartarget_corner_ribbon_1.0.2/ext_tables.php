<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetCornerRibbon', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetCornerRibbon', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetCornerRibbon\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_corner_ribbon/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_corner_ribbon/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
