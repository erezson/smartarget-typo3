<?php

return [
    'frontend' => [
        'SmartargetCornerRibbon-frontend' => [
            'target' => \Smartarget\SmartargetCornerRibbon\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
