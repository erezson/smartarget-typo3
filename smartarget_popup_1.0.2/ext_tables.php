<?php
defined('TYPO3_MODE') || die();

(function(){
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
       'Smartarget.SmartargetPopup', // Vendor dot Extension Name in CamelCase
       'web', // the main module
       'SmartargetPopup', // Submodule key
       'bottom', // Position
       [
           //'Post' => 'main',
           \Smartarget\SmartargetPopup\Controller\PostController::class => 'main',
       ],
       [
           'access' => 'admin',
           'icon'   => 'EXT:smartarget_popup/Resources/Public/Icons/favicon.png',
           'labels' => 'LLL:EXT:smartarget_popup/Resources/Private/Language/locallang.xlf',
           'navigationComponentId' => '',
           'inheritNavigationComponentFromMainModule' => false
       ]
    );

})();
