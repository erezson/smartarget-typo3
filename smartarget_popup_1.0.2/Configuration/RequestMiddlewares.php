<?php

return [
    'frontend' => [
        'SmartargetPopup-frontend' => [
            'target' => \Smartarget\SmartargetPopup\Middleware\AwesomeMiddleware::class,
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ]
];
